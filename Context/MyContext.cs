using Microsoft.EntityFrameworkCore;
using apiblazor.Models;
namespace apiblazor.Context
{
    public class MyContext : DbContext
    {
        public DbSet<Usuario> usuario { get; set; }
        public DbSet<Disciplina> disciplina { get; set; }
        public DbSet<Professor> professor { get; set; }
        public DbSet<Aluno> aluno { get; set; }
        public MyContext(DbContextOptions<MyContext> optionsBuilder) : base(optionsBuilder)
        {


        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Usuario>().HasKey(u => u.UsuarioID);
            builder.Entity<Professor>().HasKey(p => p.ProfessorID);
            builder.Entity<Disciplina>().HasKey(d => d.DisciplinaID);
            builder.Entity<Aluno>().HasKey(a => a.AlunoID);
            base.OnModelCreating(builder);
        }

    }
}