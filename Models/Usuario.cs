using System.ComponentModel.DataAnnotations;
namespace apiblazor.Models
{
    public class Usuario
    {
        [Key]
        public int UsuarioID { get;set; }
        [Required]
        public string Data { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        [Required]
        public string Username { get;set; }
        [Required]
        public string Password { get;set; }


    }

}


