using System.ComponentModel.DataAnnotations;

namespace apiblazor.Models
{
    public class Professor
    {
        [Key]
        public int ProfessorID { get; set; }
        [Required]
        public string Data { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Telefone { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Cep { get; set; }
        [Required]
        public string Idade { get; set; }
        [Required]
        public string Sexo { get; set; }
        public string Certificados { get; set; }
    }
}