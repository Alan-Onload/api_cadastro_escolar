namespace apiblazor.Models
{
    public class Disciplina
    {
        public int DisciplinaID { get; set; }
        public string Data { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public string Nome { get; set; }
        public string Nivel { get; set; }
     
    }

}
