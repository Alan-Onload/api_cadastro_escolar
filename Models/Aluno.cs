using System.ComponentModel.DataAnnotations;
namespace apiblazor.Models
{
    public class Aluno
    {
        [Key]
        public int AlunoID {get;set;}
        [Required]
        public string Data { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        [Required]
        public string Nome {get;set;}
        [Required]
        public string Telefone {get;set;}
        [Required]
        public string Email {get;set;}
        [Required]
        public string Cep {get;set;}
        [Required]
        public string Idade {get;set;}
        [Required]
        public string Sexo {get;set;}
       
        

        
    }
}