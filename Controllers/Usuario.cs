using apiblazor.Context;
using apiblazor.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace apiblazor.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class UsuarioController : ControllerBase
    {
        private readonly MyContext db;
        public UsuarioController(MyContext _db)
        {
            db = _db;
        }

        [HttpGet]
        public List<Usuario> listar()
        {

            var dados = db.usuario.ToList();
            return dados;
        }

        [HttpGet("{id}")]
        public Usuario pesquisar(int id)
        {

            var dados = db.usuario.Find(id);
            return dados;
        }

        [HttpPost]
        public int salvar(Usuario user)
        {
            db.Add(user);
            int i = db.SaveChanges();
            return i;

        }

        [HttpPut("{id}")]
        public int editar(int id, Usuario user)
        {
            var dados = db.usuario.Find(id);
            dados.Username = user.Username;
            dados.Password = user.Password;
            int i = db.SaveChanges();
            return i;
        }

        [HttpDelete("{id}")]
        public int deletar(int id)
        {
            var dados = db.usuario.Find(id);
            db.usuario.Remove(dados);
            int i = db.SaveChanges();
            return i;
        }



    }
}